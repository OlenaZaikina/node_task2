const {User} = require('../models/userModel');
const {Credentials} = require('../models/credentialsModel');
const {Note} = require('../models/noteModel');
const bcrypt = require('bcrypt');

module.exports.openProfileContloller = async (req, res) => {
  const {username} = req.user;
  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({message: `User was not found`});
  }

  res.json({
    'user': {
      '_id': user._id,
      'username': user.username,
      'createdDate': user.createdDate,
    },
  });
};

module.exports.changePasswordContloller = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const {username} = req.user;

  const dbPassword = await Credentials.findOne({username});

  if ( !(await bcrypt.compare(oldPassword, dbPassword.password)) ) {
    return res.status(400).json({message: `Wrong password`});
  }

  if (oldPassword === newPassword) {
    return res.status(400).json({message: `The same password`});
  }
  const hashedPass = await bcrypt.hash(newPassword, 10);

  await Credentials.updateOne(
      {username: username}, {$set: {password: hashedPass}}, {});

  res.json({
    'message': 'Success',
  });
};

module.exports.deleteUserProfileContloller = async (req, res) => {
  const {_id, username} = req.user;
  const user = await User.find({_id});

  if (!user) {
    return res.status(400).json({message: `User does not exist`});
  }

  await Note.deleteMany({userId: _id});
  await User.deleteMany({username});
  await Credentials.deleteMany({username});

  res.json({
    'message': 'Success',
  });
};
