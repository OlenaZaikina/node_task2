const {Note} = require('../models/noteModel');

module.exports.createNoteContloller = async (req, res) => {
  const {text} = req.body;
  const {_id} = req.user;

  if (!text) {
    return res.status(400).json({message: `Text is absent`});
  }

  const note = new Note({
    userId: _id,
    completed: false,
    text: text,
  });
  await note.save();
  res.json({message: 'Success'});
};

module.exports.readNotesContloller = async (req, res) => {
  const {_id} = req.user;
  const offset = parseInt(req.query.offset);
  const limit = parseInt(req.query.limit);

  if (limit < 1) {
    return res.status(400).json({message: `Wrong pagination`});
  }

  const notes = await Note.find({userId: _id}, {}, {offset, limit});

  if (!notes.length) {
    return res.status(200).json({'notes':''});
  }

  res.json({
    'notes': notes,
  });
};

module.exports.readNoteByIdContloller = async (req, res) => {
  const {_id} = req.user;
  const nId = req.params.id;
  const note = await Note.find({userId: _id, _id: nId}, {});

  if (!note) {
    return res.status(400).json({message: `Note does not exist`});
  }

  if (!note.length) {
    return res.status(400).json({message: `User doesn't have this note!`});
  }

  res.json({
    'note': note[0],
  });
};

module.exports.updateNoteByIdContloller = async (req, res) => {
  const {_id} = req.user;
  const nId = req.params.id;
  const {text} = req.body;
  const checkNote = await Note.find({userId: _id, _id: nId}, {});

  if (!checkNote) {
    return res.status(400).json({message: `Note does not exist`});
  }

  if (!checkNote.length) {
    return res.status(400).json({message: `User doesn't have this note!`});
  }

  await Note.updateOne({_id: nId}, {$set: {text: text}}, {});

  res.json({
    'message': 'Success',
  });
};

module.exports.checkUncheckNoteByIdContloller = async (req, res) => {
  const {_id} = req.user;
  const nId = req.params.id;

  const checkNote = await Note.find({userId: _id, _id: nId}, {});

  if (!checkNote) {
    return res.status(400).json({message: `Note does not exist`});
  }

  if (!checkNote.length) {
    return res.status(400).json({message: `User doesn't have this note!`});
  }

  const oldNote = await Note.findById({_id: nId});
  const {completed} = oldNote;
  await Note.updateOne(
      {_id: nId}, {$set: {completed: !completed}}, {});
  res.json({
    'message': 'Success',
  });
};

module.exports.deleteNoteByIdContloller = async (req, res) => {
  const {_id} = req.user;
  const nId = req.params.id;
  const checkNote = await Note.find({userId: _id, _id: nId}, {});

  if (!checkNote) {
    return res.status(400).json({message: `Note does not exist`});
  }

  if (!checkNote.length) {
    return res.status(400).json({message: `User doesn't have this note!`});
  }

  await Note.deleteOne({_id: nId});
  res.json({
    'message': 'Success',
  });
};
