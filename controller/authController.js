const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const {Credentials} = require('../models/credentialsModel');

module.exports.registrationContloller = async (req, res) => {
  const {username, password} = req.body;

  const credentions = new Credentials({
    username,
    password: await bcrypt.hash(password, 10),
  });

  const user = new User({
    username,
  });

  await credentions.save();
  await user.save();
  res.json({message: 'Success'});
};

module.exports.loginContloller = async (req, res) => {
  const {username, password} = req.body;

  const user = await Credentials.findOne({username});

  if (!user) {
    return res.status(400).json({message: `Wrong password or username`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password or username!`});
  }

  const token = jwt.sign(
      {username: user.username, _id: user._id}, JWT_SECRET);
  res.json({message: 'Success', jwt_token: token});
};
