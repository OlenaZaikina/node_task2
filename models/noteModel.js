const mongoose = require('mongoose');
const {Schema} = mongoose;

const noteSchema = new Schema({
  userId: {
    type: String,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: String,
    default: new Date().toISOString().split('.')[0] + 'Z',
  },
});

module.exports.Note = mongoose.model('Note', noteSchema);
