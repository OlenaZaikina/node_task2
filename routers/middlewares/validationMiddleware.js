const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateChangingPassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};
