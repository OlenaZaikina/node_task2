const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

module.exports.authorizationMiddleware = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    return res.status(400).json(
        {message: `Authorization header hasn't found!`});
  }

  const [tokenType, token] = authHeader.split(' ');
  console.log(tokenType);
  if (!token) {
    return res.status(400).json({message: `JWT token hasn't found!`});
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};
