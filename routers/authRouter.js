const express = require('express');
const router = new express.Router();

const {asyncHelper} = require('./helper');
const {validateRegistration, validateLogin} = require(
    './middlewares/validationMiddleware');
const {loginContloller, registrationContloller} = require(
    '../controller/authController');

router.post('/register', asyncHelper(validateRegistration), asyncHelper(
    registrationContloller));
router.post('/login', asyncHelper(validateLogin), asyncHelper(loginContloller));

module.exports = router;
