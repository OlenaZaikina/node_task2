const express = require('express');
const router = new express.Router();

const {asyncHelper} = require('./helper');
const {authorizationMiddleware} = require('./middlewares/authMiddleware');
const {createNoteContloller, readNotesContloller,
  readNoteByIdContloller, updateNoteByIdContloller,
  checkUncheckNoteByIdContloller, deleteNoteByIdContloller} = require(
    '../controller/noteController');

router.get('/notes', authorizationMiddleware, asyncHelper(
    readNotesContloller));
router.post('/notes', authorizationMiddleware, asyncHelper(
    createNoteContloller));
router.get('/notes/:id', authorizationMiddleware, asyncHelper(
    readNoteByIdContloller));
router.put('/notes/:id', authorizationMiddleware, asyncHelper(
    updateNoteByIdContloller));
router.patch('/notes/:id', authorizationMiddleware, asyncHelper(
    checkUncheckNoteByIdContloller));
router.delete('/notes/:id', authorizationMiddleware, asyncHelper(
    deleteNoteByIdContloller));

module.exports = router;
